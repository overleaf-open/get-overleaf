# Overleaf 工具箱

本仓库是Overleaf工具箱，帮助你快速运行[Overleaf](https://overleaf.com)。具体特点如下：
- 完整的Texlive2023镜像，自动化流水线发布，帮助你快速更新
- 纯净化的安装，仅仅在官方的镜像之上增加了完整的Texlive宏包
- 仓库镜像定期更新，保持和官方代码仓库的一致，方便国内访问
- 本工具包经过修改，使用的是完整版本的镜像，如果需要更换官方镜像，请自行修改或使用官方版本
- 定期更新，如果有任何的需要欢迎提出

## 快速开始
克隆本仓库：
``` sh
git clone https://gitlab.com/overleaf-open/get-overleaf.git ./overleaf-toolkit
```

> 如果权限不够，你可能需要在下面的命令里面添加sudo

然后：
``` sh
cd overleaf-toolkit
bin/up # sudo bin/up
```

等待屏幕出现大量的日志信息滚动之后，例如`sharelatex`、`mongoDB`的信息之后，按`Ctrl+C`退出，然后执行：
``` sh
bin/start # sudo bin/start
```

## 镜像信息
镜像同样发布在Gitlab中，发布地址为：[容器镜像库](https://gitlab.com/overleaf-open/overleaf/container_registry)
- `overleaf/sharelatex`：Overleaf的运行镜像，Tag为amd64-latest是完整版本的，amd64-origin的是原版本的
- `overleaf/sharelatex-base`：Overleaf的开发基础镜像，仅包含基础环境，Texlive的最小安装环境，一般不需要使用。

> Arm64的镜像有吗？
> 如果您的服务器是arm的架构，或者你需要在苹果的处理器M1上运行，目前由于Arm64的镜像目前在Texlive有些问题，可能需要后期才能上传。
